﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

	public int health = 100;

	private Color color;
	private Texture2D overlay;

	void Start() {
		color = new Color (0.85f, 0.3f, 0.3f, 0.0f);
		overlay = new Texture2D(1, 1);
		refreshOverlay (color);
	}

	public void OnGUI() {
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), overlay);
	}

	public void OnTriggerEnter(Collider other) {
		color.a = 0.8f;
		refreshOverlay (color);
	}

	public void OnTriggerExit(Collider other) {
		color.a = 0.0f;
		refreshOverlay (color);
	}

	public void refreshOverlay(Color c) {
		overlay.SetPixel (0, 0, c);
		overlay.Apply ();
	}
}

﻿using UnityEngine;
using System.Collections;

public class LaserTarget {
	private Vector3 origin;

	private Vector3 start;
	private Vector3 end;

	private GameObject target;
	private float range = 4f;
	private float progress;
	private float offset = 0.5f;

	public LaserTarget(GameObject obj) {
		target = obj;
		origin = obj.transform.localPosition;
		offset += range / 2f;
	}

	public void Move(float delta) {
		progress += delta;
		if (progress >= 1.0f) {
			progress = 1.0f;
			Retarget ();
		}
		target.transform.localPosition = origin + Vector3.Lerp (start, end, progress);
	}

	void Retarget() {
		start = end;
		float v = 1.0f * range;
		if (Random.value > 0.5) {
			v = 0f;
		}
		end = new Vector3(0f, offset - v, 0f);
		progress = 0f;
	}
}

[RequireComponent(typeof(LineRenderer))]
public class Laser : MonoBehaviour {
	public GameObject A;
	public GameObject B;

	public float range = 30f;
	public float targetDuration = 3f; 

	public Vector3 vector;
    private GameObject player;

    private LineRenderer beam;

	private Vector3 start;
	private LaserTarget targetA;
	private LaserTarget targetB;

	private bool isTriggering;

	void Start () {
		beam = GetComponent<LineRenderer> ();
		targetA = new LaserTarget (A);
		targetB = new LaserTarget (B);
		start = this.transform.position;

        player = GameObject.FindGameObjectWithTag("Player");
    }

    bool CheckCollision(Ray ray, float distance)
    {
        if (player == null)
        {
            return false;
        }
        // TODO: Replace with a custom collider?
        RaycastHit hit;
        if (player.GetComponent<MeshCollider>().Raycast(ray, out hit, distance))
        {
            player.GetComponent<Character>().OnTriggerEnter(this.GetComponent<Collider>());
            isTriggering = true;
        }
        else if (isTriggering)
        {
            player.GetComponent<Character>().OnTriggerExit(this.GetComponent<Collider>());
            isTriggering = false;
        }
        return isTriggering;
    }

	void Update() {
		this.transform.position += vector * Time.deltaTime;

		targetA.Move (Time.deltaTime);
		targetB.Move (Time.deltaTime);

		beam.SetPosition (0, A.transform.localPosition);
		beam.SetPosition (1, B.transform.localPosition);

		Ray ray = new Ray (A.transform.position, B.transform.localPosition-A.transform.localPosition);
		float distance = Vector3.Distance (B.transform.localPosition, A.transform.localPosition);

        CheckCollision(ray, distance);

		if (Vector3.Distance(start, this.transform.position) > range) {
			Destroy (this);
			Destroy (this.gameObject);
		}
	}
}
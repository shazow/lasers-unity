﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject template;
	private Vector3 startPos;
	private Quaternion startRot;

	public int count = 3;
	public float interval = 3f;

	private int numSpawned = 0;
	private float timeSpawn = 0f;

	void Start() {
		startPos = template.transform.localPosition;
		startRot = template.transform.localRotation;
	}

	// Update is called once per frame
	void Update () {
		if (count <= numSpawned || timeSpawn > Time.time) {
			return;
		}

		Object.Instantiate (template, startPos, startRot);
		timeSpawn = Time.time + interval;
		numSpawned++;
	}
}
